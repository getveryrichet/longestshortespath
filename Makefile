all : GPU Parallel

GPU :
	nvcc -Xcompiler -fopenmp -lgomp johnson_gpu_16000.cu -o johnson_16000
	nvcc -Xcompiler -fopenmp -lgomp floyd_gpu_32000.cu -o floyd_32000

Parallel :
	g++ -std=c++11 -fopenmp -mcmodel=medium dijkstra_parallel_1000000.cpp -o dijkstra_1000000

	
clean :
	rm -f *.o
	rm -f dijkstra_1000000
	rm -f floyd_32000
	rm -f johnson_16000

