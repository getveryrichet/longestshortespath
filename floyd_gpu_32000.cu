#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <chrono> 
#include <cassert>
#include <fstream>
#include <string>
#include <sstream>

#define BLOCK_SIZE 32
using namespace std;
using namespace std::chrono; 

// Kernel function for inner two loop of Floyd Warshall Algorithm



  
const int V = 32000;
  
const int INF = 99999;  

void readgraph(int *dist){
    
    string directory = ".";
    string file_name = "32000.graph";
    string filePath = directory + "/" + file_name;
    string str;

    ifstream openFile(filePath.data());
    if(openFile.fail()){
        printf("read_program_파일 여는데 실패 \n");
    }  
    bool read = true;

    while(getline(openFile, str)) 
    {
        istringstream ss(str);

        int src;
        int dest;
        int weight;
        // cout << ss << endl;
        ss >> src >> dest >> weight;

        dist[src*V + dest] = weight;
        if (read) {
          printf("read first line\n");
          read = false;
        }
    }
    printf("finished reading graph \n");
}

__global__
void GPUInnerLoops(int V, int k, int *dis)
{
	// int v1 = blockDim.y * blockIdx.y + threadIdx.y;
	// int v2 = blockDim.x * blockIdx.x + threadIdx.x;
  int v1 = blockIdx.y * BLOCK_SIZE + threadIdx.y;
  int v2 = blockIdx.x * BLOCK_SIZE + threadIdx.x;
  if (v1 < V && v2 < V) {
    int newPath = dis[v1 * V + k] + dis[k * V + v2];
    int oldPath = dis[v1 * V + v2];
    if (oldPath > newPath)
    {
      dis[v1 * V + v2] = newPath;
    }
  }

} 

void FloydWarshall(int *dis)
{
    
    unsigned int grid_rows = (V + BLOCK_SIZE - 1) / BLOCK_SIZE;
    unsigned int grid_cols = (V + BLOCK_SIZE - 1) / BLOCK_SIZE;

    dim3 dimGrid(grid_cols, grid_rows);
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
 
		for (int k = 0; k < V; k++)  
  	{
				GPUInnerLoops<<<dimGrid, dimBlock>>>(V, k, dis);
				cudaDeviceSynchronize();
		}
}


int main(void)
{
	
    int *dist;
    int n = V;
    // Allocate Unified Memory – accessible from CPU or GPU
    cudaMallocManaged(&dist, V*V*sizeof(int));


    // initialize dis array on the host

    #pragma omp parallel for
    for(int i=0;i<n;i++){
        #pragma omp parallel for
        for(int j=0;j<n;j++){
            if((i*n)==j){
                // dist[i][j] = 0;
                dist[i*n + j] = 0;
            }
            else{
                dist[i*n + j] = INF;
            }
        }
    }
    printf("intialized distance matrix \n");

    readgraph(dist);

    int read_count = 0;
    for (int i = 0; i < V; i++)
    {   
        for (int j = 0; j < V; j++)
        {
            // int current_dist = dist[i*k + j];
            if(dist[i*V + j] != 0 && dist[i*V + j] != INF){
                printf("[%d][%d]:%d  ", i, j, dist[i*V + j]);
                read_count = read_count + 1;
            }

            
        }
        if (read_count > 10){
        printf("checked reading graph \n");
        break;
        }
        //printf("\n");
    }
    auto start_time = high_resolution_clock::now(); 

    printf("Start FloydWarshall \n");

    FloydWarshall(dist);

    printf("Done FloydWarshall \n");

    printf("Finding Longest Shortest Path \n");
    int max_dist = 0;
    int max_src = 0;
    int max_dest = 0;

    for (int i = 0; i < V; i++)
    {   
        for (int j = 0; j < V; j++)
        {
            int current_dist = dist[i*V + j];
            if(current_dist > max_dist && current_dist != INF){
                max_dist = current_dist;
                max_src = i;
                max_dest = j;
            }
        }
        //printf("\n");
    }
    printf("====== result ==== \n");
    printf("max_dist from %d to %d weight = %d \n", max_src, max_dest, max_dist);
    auto stop_time = high_resolution_clock::now();

    auto duration = duration_cast<std::chrono::milliseconds>(stop_time - start_time); 
    printf("Time taken overall in milliseconds(ms) : %ld \n", duration.count());
    // Free memory
    cudaFree(dist);
    return 0;
}