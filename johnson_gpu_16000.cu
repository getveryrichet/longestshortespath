
 #include <stdio.h>
 #include <stdlib.h>
 #include <assert.h>
 #include <iostream>
 #include <random>  
 #include <chrono>
 #include <fstream>
 #include <vector>
 #include <iostream>
 #include <fstream>
 #include <string>
 #include <sstream>
 #include <vector>
 #include <queue>
 #include <string.h>
 #include <omp.h>
 #include <iostream>
 #include <algorithm>
 #include <limits>
 using namespace std::chrono; 
 using namespace std;
 
 #define BLOCK_SIZE 32
 

 __global__ void gpu_matrix_mult(int *a,int *b, int *c, int m, int n, int k)
 { 
     int row = blockIdx.y * blockDim.y + threadIdx.y; 
     int col = blockIdx.x * blockDim.x + threadIdx.x;
     int sum = 0;
     int tmp = c[row * k + col];

     if( col < k && row < m) 
     {
         for(int i = 0; i < n; i++) 
         {
            int challenge = a[row * n + i] + b[i * k + col];
            if (challenge < tmp && challenge >=0) {
                tmp = challenge;
            }
         }
         c[row * k + col] = tmp;
     }
 } 
 

int dtn(int n, int min_n) 
 {
     int max_tn = n / min_n;
     const int g_ncore = omp_get_num_procs();
     int tn = max_tn > g_ncore ? g_ncore : max_tn; 
     if(tn < 1)
     {
         tn = 1; 
     }
     return tn; 
 }

void omp_mm(int *a, int row_a, int col_a, int *b, int row_b,int col_b, int *c)
{
    if ( col_a != row_b ) 
    {
        return; 
    }
    int i, j, k;
    int index;
    int border = row_a * col_b;
    i = 0;
    j = 0;

    #pragma omp parallel for private(i,j,k) num_threads(dtn(border, 1))
    for ( index = 0; index < border; index++ ) 
    {
        i = index / col_b; j = index % col_b;
        int row_i = i * col_a; 
        int row_c = i * col_b;

        int tmp = 999999;

        for ( k = 0; k < row_b; k++ ) 
        {
            int challenge = a[row_i+k] * b[k*col_b+j];
            if (challenge < tmp and challenge >= 0) {
                tmp = challenge;
            }
            
        }
        c[row_c + j] = tmp;

    } 
}
 

const int size = 16000;
int dist[size*size];
const int INF = 999999;

int main(int argc, char const *argv[]){
    
    int m, n, k;
    m = size;
    n = size;
    k = size;

    // int *dist;

    int *h_a, *h_b, *h_c, *h_cc;
    int *d_a, *d_b, *d_c;


    
    printf("initializing matrix");

	omp_set_dynamic(0);
    omp_set_num_threads(omp_get_num_procs());

    // initialize matrix
    #pragma omp parallel for
    for(int i=0;i<n;i++){
        #pragma omp parallel for
        for(int j=0;j<n;j++){
            if(i==j){
                // dist[i][j] = 0;
                dist[i*n + j] = 0;
            }
            else{
                dist[i*n + j] = INF;
            }
        }
    }
    printf("initialized matrix \n");

    string directory = ".";
    string file_name = "16000.graph";
    string filePath = directory + "/" + file_name;
    string str;

    ifstream openFile(filePath.data());
    if(openFile.fail()){
        printf("read_program_파일 여는데 실패 \n");
    }  

    while(getline(openFile, str)) 
    {
        istringstream ss(str);

        int src;
        int dest;
        int weight;
        ss >> src >> dest >> weight;
        // cout << "src = " << src << "dest = " << dest << "weight = " << weight << endl;
        // dist[src][dest] = weight;
        dist[src*n + dest] = weight;
    }
    printf("read graph \n");

    
    cudaMallocHost((void **) &h_a, sizeof(int)*m*n);
    cudaMallocHost((void **) &h_c, sizeof(int)*m*k);
    cudaMallocHost((void **) &h_cc, sizeof(int)*m*k);
    #pragma omp parallel for
    for (int i = 0; i < m; i++) {
        #pragma omp parallel for
        for (int j = 0; j < n; j++) {
            // h_a[(i * n) + j] = dist[i][j];
            h_a[(i * n) + j] = dist[i*n +j];
        }
    }
    printf("inialize h_a \n");

    #pragma omp parallel for
    for (int i = 0; i < n; i++) {
        #pragma omp parallel for
        for (int j = 0; j < k; j++) {
            if (i==j){
                h_c[(i * k) + j] = 0;
            }
            else { h_c[(i * k) + j] = INF;}
        }
    }


    printf("gpu start \n");
 
    float gpu_elapsed_time_ms, cpu_elapsed_time_ms;

    cudaEvent_t start, stop, start_squaring, stop_squaring;
    auto start_time = high_resolution_clock::now(); 
    cudaMalloc((void **) &d_a, sizeof(int)*m*n);
    cudaMalloc((void **) &d_c, sizeof(int)*m*k);

    cudaMemcpy(d_a, h_a, sizeof(int)*m*n, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, h_a, sizeof(int)*m*n, cudaMemcpyHostToDevice);
    cudaMemcpy(d_c, h_c, sizeof(int)*n*k, cudaMemcpyHostToDevice);

    unsigned int grid_rows = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
    unsigned int grid_cols = (k + BLOCK_SIZE - 1) / BLOCK_SIZE;

    dim3 dimGrid(grid_cols, grid_rows);
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

    float squaring = 1;

    while ((int)squaring < size) {
    // Launch kernel 
        cudaEventCreate(&start_squaring);
        cudaEventCreate(&stop_squaring);
        cudaEventRecord(start_squaring, 0);
        gpu_matrix_mult<<<dimGrid, dimBlock>>>(d_a, d_a, d_c, m, n, k); 
        cudaMemcpy(d_a, d_c, sizeof(int)*m*n, cudaMemcpyHostToDevice); 
        cudaMemcpy(d_c, h_c, sizeof(int)*n*k, cudaMemcpyHostToDevice); 
        cudaThreadSynchronize();
        cudaEventRecord(stop_squaring, 0);
        cudaEventSynchronize(stop_squaring);
        cudaEventElapsedTime(&gpu_elapsed_time_ms, start_squaring, stop_squaring);
        printf("Time elapsed on matrix multiplication of %dx%d . %dx%d on GPU: %f ms.\n\n", m, n, n, k, gpu_elapsed_time_ms);
        squaring = squaring * 2.0;
        printf("squaring = %f \n", squaring);        
    }    
    printf("squaring_finished \n");

    cudaMemcpy(h_c, d_a, sizeof(int)*m*k, cudaMemcpyDeviceToHost);

    auto stop_time_gpu = high_resolution_clock::now();

    auto duration_gpu = duration_cast<std::chrono::milliseconds>(stop_time_gpu - start_time); 

    printf("Time taken to calculate by gpu in milliseconds(ms) : %ld \n", duration_gpu.count());
    
    printf("Finding Longest Shortest Path \n");
    int max_dist = 0;
    int max_src = 0;
    int max_dest = 0;

    for (int i = 0; i < m; i++)
    {   
        for (int j = 0; j < k; j++)
        {
        //  printf("[%d][%d]:%d == [%d][%d]:%d \n", i, j, h_cc[i*k + j], i, j, h_c[i*k + j]);
            int current_dist = h_c[i*k + j];
            if(current_dist > max_dist && current_dist != INF){
                max_dist = current_dist;
                max_src = i;
                max_dest = j;
                // printf("updated from %d to %d weight = %d", max_src, max_dest, max_dist);
            }
        }
    }

    printf("====== result ==== \n");
    printf("max_dist from %d to %d weight = %d \n", max_src, max_dest, max_dist);
    auto stop_time = high_resolution_clock::now();

    auto duration = duration_cast<std::chrono::milliseconds>(stop_time - start_time); 
    printf("Time taken overall in milliseconds(ms) : %ld \n", duration.count());
    // free memory
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    cudaFreeHost(h_a);
    cudaFreeHost(h_b);
    cudaFreeHost(h_c);
    cudaFreeHost(dist);
    // cudaFreeHost(h_cc);

    printf("read graph \n");
    return 0;
 }