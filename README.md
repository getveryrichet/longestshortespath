# Find Longest Shortest Path

## Introduction

In order to find longest shortest path in graph with 16000, 32000, and 1000000 vertices, I tried to implement three different algorithm to understand what I've learned so far.

### graph with 16,000 vertices

For graph with 16000 vertices, I implemented Johnson's algorithm with cuda in order to utilize GPU to do my own matrix multiplication. 

### graph with 32,000 vertices

For graph with 32000 vertices, I implemented Floyd-Warshall algorithm with cuda in order to utilize GPU to do my own matrix multiplication

### graph with 1,000,000 vertices

For graph with 1000000 vertices, I implemented Dijkstra algorithm with c++ using OpenMP in order to do multi-threading while searching for shortest path for each vertices.

## How to run my code

1) clone my gitlab repository

[EunTaekOh / LongestShortesPath](https://gitlab.com/getveryrichet/longestshortespath.git)

2) make to compile my code

```bash
$ make
```

![Find%20Longest%20Shortest%20Path%20ac7429b7c0c24bf6b9d553039d20aa0d/Untitled.png](Find%20Longest%20Shortest%20Path%20ac7429b7c0c24bf6b9d553039d20aa0d/Untitled.png)

3) you need to have nvcc (nvidia cuda tool kit) installed with GPU

---

## Find Longest Shortest Path

### Run for 16000 graph

- I implemented johnson's algorithm with cuda in order to calculate using GPU

**Result**

![Find%20Longest%20Shortest%20Path%20ac7429b7c0c24bf6b9d553039d20aa0d/Untitled%201.png](Find%20Longest%20Shortest%20Path%20ac7429b7c0c24bf6b9d553039d20aa0d/Untitled%201.png)

- Longest Shortest Path
    - source : 12657
    - dest : 4569
    - weight : 107

---

### Run for 32000 graph

- I implemented floyd-warshall algorithm with cuda in order to calculate using GPU

**Result**

![Find%20Longest%20Shortest%20Path%20ac7429b7c0c24bf6b9d553039d20aa0d/Untitled%202.png](Find%20Longest%20Shortest%20Path%20ac7429b7c0c24bf6b9d553039d20aa0d/Untitled%202.png)

- Longest Shortest Path
    - source : 28850
    - dest : 12334
    - weight : 131

---

### Run for 1000000 graph

- I implemented Dijkstra's algorithm with c++  in order to do multi-threading for each vertex's iteration

**Result**

![Find%20Longest%20Shortest%20Path%20ac7429b7c0c24bf6b9d553039d20aa0d/Untitled%203.png](Find%20Longest%20Shortest%20Path%20ac7429b7c0c24bf6b9d553039d20aa0d/Untitled%203.png)

- Longest Shortest Path
    - source : 842487
    - dest : 252404
    - weight : 197