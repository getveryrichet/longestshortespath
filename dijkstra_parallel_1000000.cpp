
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <chrono> 
#include <cassert>
#include <fstream>
#include <string>
#include <sstream>
#include<vector>
#include <array>
#include<queue>
#include <omp.h>
#define INF 1e9
using namespace std;
using namespace std::chrono; 


const int V = 1000000;
int max_arr[V+1][3];
vector<pair<int,int> > arr[V+1];

int main(void)
{
  string directory = ".";
  string file_name = "1000000.graph";
  string filePath = directory + "/" + file_name;
  string str;

  ifstream openFile(filePath.data());
  if(openFile.fail()){
      printf("read_program_파일 여는데 실패 \n");
  }  

  while(getline(openFile, str)) 
  {
      istringstream ss(str);

      int src;
      int dest;
      int weight;
      // cout << ss << endl;
      ss >> src >> dest >> weight;

      arr[src].push_back({dest, weight});
      // cout << "src "<< src;
      // cout << "src = " << src << "dest = " << dest << "weight = " << weight << endl;
      // dist[src][dest] = weight;
      
      // cout << "after first_line " << lines.programname  << " " <<  lines.cycle << endl;
  }
  printf("read graph \n");



	omp_set_dynamic(0);
  omp_set_num_threads(omp_get_num_procs());

  #pragma omp parallel for
  for(int start = 0; start < V; start++){
    auto start_time = high_resolution_clock::now(); 
    int max_src = -1;
    int max_dest = -1;
    int max_weight = -1;

    int dist[V+1];    //최단거리를 갱신해주는 배열입니다. 
    fill(dist,dist+V+1,INF);    //먼저 무한대로 전부 초기화를 시켜둡니다. 
    priority_queue<pair<int,int> > qu;    



    qu.push({0,start});    //우선순위 큐에 시작점을 넣어줍니다. 
    dist[start]=0;    //시작점의 최단거리를 갱신합니다. 
    
    while(!qu.empty()){
        int cost=-qu.top().first;    // cost는 다음 방문할 점의 dist값을 뜻합니다. 
        int here=qu.top().second;     // here을 방문할 점의 번호를 뜻합니다 
        
        qu.pop();
            
        for(int i=0; i<arr[here].size(); i++){
            int next=arr[here][i].first;
            int nextcost=arr[here][i].second;
            
            if(dist[next] > dist[here] + nextcost){    
                //현재 next에 저장된 dist의값보다 현재의 점을 거쳐서 갈 경우가 
                // 거리가 더짧으면 갱신해 주고 큐에 넣습니다. 
                dist[next]=dist[here]+nextcost;
                qu.push({-dist[next],next});
            }
        }
        
    }
    // #pragma omp parallel for
    for(int i=1;i<=V;i++){
      int weight = dist[i];
      if(weight != INF && weight != 0){
        if(weight > max_weight){
          max_src = start;
          max_dest = i;
          max_weight = weight;
          // printf("max weight updated from %d to %d weight = %d \n", max_src, max_dest, max_weight);
        }
      }
    }
    max_arr[start][0] = max_src;
    max_arr[start][1] =max_dest;
    max_arr[start][2] =max_weight;
    auto stop_time = high_resolution_clock::now();

    auto duration = duration_cast<std::chrono::milliseconds>(stop_time - start_time); 

    ofstream myfile;
    myfile.open ("dist.txt", ios::out | ios::app);
    myfile << max_src << " " << max_dest << " " << max_weight << "\n";
    myfile.close();
  }
  int max_src;
  int max_dest;
  int max_weight;
  int src;
  int dest;
  int weight;

  for(int i = 0; i < V; i ++){
    src = max_arr[i][0];
    dest = max_arr[i][1];
    weight = max_arr[i][2]; 
    if(weight > max_weight){ 
      max_src = src;
      max_dest = dest;
      max_weight = weight;
    }
  }
  printf("max weight updated from %d to %d weight = %d \n", max_src, max_dest, max_weight);
  cout << "done" << endl;

  return 0;



}
